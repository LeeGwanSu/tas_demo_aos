package com.humuson.tasdemo.presenter;

import android.text.TextUtils;
import android.view.View;

import com.apms.sdk.APMS;
import com.apms.sdk.TAS;
import com.apms.sdk.api.APIManager;
import com.apms.sdk.api.request.LoginPms;
import com.google.android.material.snackbar.Snackbar;
import com.humuson.tasdemo.view.MainActivity;

import androidx.appcompat.app.ActionBar;

import org.json.JSONObject;

public class MainPresenter
{
    private MainActivity activity;

    public MainPresenter(MainActivity activity)
    {
        this.activity = activity;
    }

    public void hideStatusBar()
    {
        ActionBar actionBar = activity.getSupportActionBar();
        if(actionBar!=null)
        {
            actionBar.hide();
        }
        android.app.ActionBar actionBarLegacy = activity.getActionBar();
        if(actionBarLegacy!=null)
        {
            actionBarLegacy.hide();
        }
    }

    public void showSnackBar(String text, View container)
    {
        Snackbar snackbar = Snackbar.make(container, text, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }
    public void initialize()
    {
        APMS apms = new APMS.Builder()
                .setDebugEnabled(true)
                .setDebugTag("APMS")
                .setServerAppKey("android2")
                .setServerUrl("https://api.pushpia.com:444/")
                .setFirebaseSenderId("997255892867")
                .build(activity.getApplicationContext());
        TAS tas = new TAS.Builder()
                .setServerUrl("https://trk-amc.tason.com/amc.gif")
                .setChannelKey(getChannelKey())
                .build(activity.getApplicationContext());
    }
    public String getChannelKey()
    {
        String channelKey = TAS.getInstance(activity.getApplicationContext()).getChannelKey();
        if(TextUtils.isEmpty(channelKey))
        {
            TAS.getInstance(activity.getApplicationContext()).setChannelKeys("3cc50922-0495-4e19-965d-ab201bb83a6a");
            return "3cc50922-0495-4e19-965d-ab201bb83a6a";
        }
        else
        {
            return channelKey;
        }
    }
}