package com.humuson.tasdemo.presenter;

import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.google.android.material.snackbar.Snackbar;
import com.humuson.tasdemo.view.AccountEventActivity;

import java.util.ArrayList;
import java.util.List;

public class AccountEventPresenter
{
    public static final int EVENT_SIGNUP = 0;
    public static final int EVENT_USERINFOMODIFY = 1;
    private int selectedEvent;

    private AccountEventActivity activity;

    public AccountEventPresenter(AccountEventActivity activity)
    {
        this.activity = activity;
    }
    public List<String> getEventList()
    {
        List<String> list = new ArrayList<>();
        list.add("SignUp");
        list.add("UserInfoModify");
        return list;
    }
    public ArrayAdapter<String> getSpinnerAdapter(List<String> apiList)
    {
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, apiList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return spinnerAdapter;
    }
    public AdapterView.OnItemSelectedListener getSpinnerSelectListener()
    {
        return new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                selectedEvent = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        };
    }
    public void showSnackBar(String text, View container)
    {
        Snackbar snackbar = Snackbar.make(container, text, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    public int getSelectedEvent()
    {
        return selectedEvent;
    }
}
