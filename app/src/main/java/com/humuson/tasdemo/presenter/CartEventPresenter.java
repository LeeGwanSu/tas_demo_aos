package com.humuson.tasdemo.presenter;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.google.android.material.snackbar.Snackbar;
import com.humuson.tasdemo.view.CartEventActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CartEventPresenter
{
    public static final int EVENT_CART = 0;
    public static final int EVENT_CART_ABANDON = 1;
    private int selectedEvent;

    private CartEventActivity activity;

    public CartEventPresenter(CartEventActivity activity)
    {
        this.activity = activity;
    }

    public List<String> getEventList()
    {
        List<String> list = new ArrayList<>();
        list.add("Cart");
        list.add("CartAbandon");
        return list;
    }
    public ArrayAdapter<String> getSpinnerAdapter(List<String> apiList)
    {
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, apiList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return spinnerAdapter;
    }
    public AdapterView.OnItemSelectedListener getSpinnerSelectListener()
    {
        return new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                selectedEvent = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        };
    }

    public int getSelectedEvent()
    {
        return selectedEvent;
    }

    public void showSnackBar(String text, View container)
    {
        Snackbar snackbar = Snackbar.make(container, text, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }
    public List<String> getTagList(String text)
    {
        text = text.replace(" ", "");
        String[] list = text.split(",");
        return Arrays.asList(list);
    }
    public List<String> getCategoryList(String text)
    {
        text = text.replace(" ", "");
        String[] list = text.split(",");
        return Arrays.asList(list);
    }
}
