package com.humuson.tasdemo.presenter;

import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.humuson.tasdemo.view.PurchaseEventActivity;

import java.util.Arrays;
import java.util.List;

public class PurchaseEventPresenter
{
    private PurchaseEventActivity activity;

    public PurchaseEventPresenter(PurchaseEventActivity activity)
    {
        this.activity = activity;
    }
    public void showSnackBar(String text, View container)
    {
        Snackbar snackbar = Snackbar.make(container, text, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }
    public List<String> getTagList(String text)
    {
        text = text.replace(" ", "");
        String[] list = text.split(",");
        return Arrays.asList(list);
    }
    public List<String> getCategoryList(String text)
    {
        text = text.replace(" ", "");
        String[] list = text.split(",");
        return Arrays.asList(list);
    }
}
