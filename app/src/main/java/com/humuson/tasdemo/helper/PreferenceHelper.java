package com.humuson.tasdemo.helper;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper
{
    private static PreferenceHelper instance;
    private static Context contextInstance;
    private static SharedPreferences.Editor editor;
    private static SharedPreferences preferences;
    private PreferenceHelper() { }
    public static PreferenceHelper getInstance(Context context)
    {
        if(instance == null)
        {
            instance = new PreferenceHelper();
        }
        contextInstance = context;
        String version = "1.0";
        editor = contextInstance.getSharedPreferences(version, Context.MODE_PRIVATE).edit();
        preferences = contextInstance.getSharedPreferences(version, Context.MODE_PRIVATE);
        return instance;
    }
    public void clearPreference()
    {
        editor.clear();
        editor.commit();
    }
    public void setUserId(String text)
    {
        editor.putString("UserId",text);
        editor.commit();
    }
    public String getUserId()
    {
        return preferences.getString("UserId", "");
    }
}
