package com.humuson.tasdemo.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.apms.sdk.TAS;
import com.humuson.tasdemo.R;
import com.humuson.tasdemo.presenter.MainPresenter;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity
{
    @BindView(R.id.main_edittext_channelKey) EditText editTextChannelKey;
    @BindView(R.id.main_linearlayout_container) LinearLayout linearLayoutContainer;

    private MainPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        presenter = new MainPresenter(this);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);
        editTextChannelKey.setText(presenter.getChannelKey());
        presenter = new MainPresenter(this);
        presenter.initialize();
    }

    @OnClick({
            R.id.main_button_login,
            R.id.main_button_cart,
            R.id.main_button_custom,
            R.id.main_button_purchase,
            R.id.main_button_channelKeyChange
             }) void onClick(View view)
    {
        Intent intent;
        switch (view.getId())
        {
            case R.id.main_button_login:
                intent = new Intent(this, AccountEventActivity.class);
                startActivity(intent);
                break;
            case R.id.main_button_cart:
                intent = new Intent(this, CartEventActivity.class);
                startActivity(intent);
                break;
            case R.id.main_button_custom:
                intent = new Intent(this, CustomEventActivity.class);
                startActivity(intent);
                break;
            case R.id.main_button_purchase:
                intent = new Intent(this, PurchaseEventActivity.class);
                startActivity(intent);
                break;
            case R.id.main_button_channelKeyChange:
                if(TextUtils.isEmpty(editTextChannelKey.getText()))
                {
                    presenter.showSnackBar("내용이 비어있습니다 값을 입력해주세요", linearLayoutContainer);
                }
                else
                {
                    TAS.getInstance(getApplicationContext()).setChannelKeys(editTextChannelKey.getText().toString().trim());
                    presenter.showSnackBar("TAS ChannelKey가 변경되었습니다", linearLayoutContainer);
                }
                break;
        }
    }
}
