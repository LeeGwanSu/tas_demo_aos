package com.humuson.tasdemo.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.apms.sdk.TAS;
import com.apms.sdk.api.APIManager;
import com.apms.sdk.bean.TASModelProduct;
import com.apms.sdk.bean.TASModelPurchase;
import com.humuson.tasdemo.R;
import com.humuson.tasdemo.helper.PreferenceHelper;
import com.humuson.tasdemo.presenter.PurchaseEventPresenter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PurchaseEventActivity extends AppCompatActivity
{
    @BindView(R.id.purchase_edittext_productName) EditText editTextProductName;
    @BindView(R.id.purchase_edittext_productAmount) EditText editTextProductAmount;
    @BindView(R.id.purchase_edittext_productCode) EditText editTextProductCode;
    @BindView(R.id.purchase_edittext_productPrice) EditText editTextProductPrice;
    @BindView(R.id.purchase_edittext_productUrl) EditText editTextProductUrl;
    @BindView(R.id.purchase_edittext_productImgUrl) EditText editTextProductImgUrl;
    @BindView(R.id.purchase_edittext_totalPrice) EditText editTextTotalPrice;
    @BindView(R.id.purchase_edittext_tags) EditText editTextTags;
    @BindView(R.id.purchase_edittext_categories) EditText editTextCategories;
    @BindView(R.id.purchase_linearlayout_container) LinearLayout linearLayoutContainer;

    private TAS tas;
    private PurchaseEventPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_purchase_activity);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter = new PurchaseEventPresenter(this);
        tas = TAS.getInstance(getApplicationContext());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    @OnClick(R.id.purchase_button_cartEventCall) void onClick()
    {
        if(validateEvent())
        {
            TASModelProduct product = new TASModelProduct.Builder()
                    .setProductImg(editTextProductImgUrl.getText().toString())
                    .setProductUrl(editTextProductUrl.getText().toString())
                    .setProductPrice(editTextProductPrice.getText().toString())
                    .setProductCode(editTextProductCode.getText().toString())
                    .build(editTextProductName.getText().toString());
            List<TASModelProduct> productList = new ArrayList<>();
            productList.add(product);
            TASModelPurchase param = new TASModelPurchase.Builder()
                    .setProducts(productList)
                    .setTags(presenter.getTagList(editTextTags.getText().toString()))
                    .setCategories(presenter.getCategoryList(editTextCategories.getText().toString()))
                    .setUserId(PreferenceHelper.getInstance(getApplicationContext()).getUserId())
                    .build(editTextTotalPrice.getText().toString());
            tas.tasPurchaseEvent(param, new APIManager.APICallback() {
                @Override
                public void response(String s, JSONObject jsonObject)
                {
                    presenter.showSnackBar("성공 ["+s+"]",linearLayoutContainer);
                }
            });
        }
        else
        {
            presenter.showSnackBar("productName과 totalPrice 값을 입력해주세요", linearLayoutContainer);
        }
    }
    private boolean validateEvent()
    {
        if(!TextUtils.isEmpty(editTextProductName.getText().toString()) && !TextUtils.isEmpty(editTextTotalPrice.getText().toString()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
