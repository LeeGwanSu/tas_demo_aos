package com.humuson.tasdemo.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.apms.sdk.TAS;
import com.apms.sdk.api.APIManager;
import com.apms.sdk.bean.TASModelCustom;
import com.humuson.tasdemo.R;
import com.humuson.tasdemo.helper.PreferenceHelper;
import com.humuson.tasdemo.presenter.CustomEventPresenter;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomEventActivity extends AppCompatActivity
{
    @BindView(R.id.custom_linearlayout_container) LinearLayout linearLayoutContainer;
    @BindView(R.id.custom_edittext_pageName) EditText editTextPageName;
    @BindView(R.id.custom_edittext_value) EditText editTextValue;
    @BindView(R.id.custom_edittext_eventName) EditText editTextEventName;
    @BindView(R.id.custom_edittext_tags) EditText editTextTags;
    @BindView(R.id.custom_edittext_category) EditText editTextCategory;
    @BindView(R.id.custom_edittext_key1) EditText editTextKey1;
    @BindView(R.id.custom_edittext_key2) EditText editTextKey2;
    @BindView(R.id.custom_edittext_value1) EditText editTextValue1;
    @BindView(R.id.custom_edittext_value2) EditText editTextValue2;

    private CustomEventPresenter presenter;
    private TAS tas;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_custom_activity);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter = new CustomEventPresenter(this);
        tas = TAS.getInstance(getApplicationContext());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick
            ({
                     R.id.custom_button_pageChangeEventCall,
                     R.id.custom_button_customEventCall
            }) void onClick(View view)
    {
        Map<String,String> map = new HashMap<>();
        if(validateCustomData1())
        {
            map.put(editTextKey1.getText().toString().trim(), editTextValue1.getText().toString().trim());
        }
        if(validateCustomData2())
        {
            map.put(editTextKey2.getText().toString().trim(), editTextValue2.getText().toString().trim());
        }

        switch (view.getId())
        {
            case R.id.custom_button_pageChangeEventCall:
                if(validateCustomPageEvent())
                {
                    TASModelCustom param = new TASModelCustom.Builder()
                            .setValue(editTextPageName.getText().toString())
                            .setData(map)
                            .setCategory(editTextCategory.getText().toString())
                            .setTags(presenter.getTagList(editTextTags.getText().toString()))
                            .setUserId(PreferenceHelper.getInstance(getApplicationContext()).getUserId())
                            .build("view");
                    tas.tasCustom(param, new APIManager.APICallback() {
                        @Override
                        public void response(String s, JSONObject jsonObject)
                        {
                            presenter.showSnackBar("성공 ["+s+"]",linearLayoutContainer);
                        }
                    });
                }
                else
                {
                    presenter.showSnackBar("pageName 값을 입력해주세요", linearLayoutContainer);
                }
                break;
            case R.id.custom_button_customEventCall:
                if(validateCustomEvent())
                {
                    TASModelCustom param = new TASModelCustom.Builder()
                            .setValue(editTextValue.getText().toString())
                            .setData(map)
                            .setCategory(editTextCategory.getText().toString())
                            .setTags(presenter.getTagList(editTextTags.getText().toString()))
                            .setUserId(PreferenceHelper.getInstance(getApplicationContext()).getUserId())
                            .build(editTextEventName.getText().toString());
                    tas.tasCustom(param, new APIManager.APICallback() {
                        @Override
                        public void response(String s, JSONObject jsonObject)
                        {
                            presenter.showSnackBar("성공 ["+s+"]",linearLayoutContainer);
                        }
                    });
                }
                else
                {
                    presenter.showSnackBar("eventName 값을 입력해주세요", linearLayoutContainer);
                }
                break;
        }
    }
    private boolean validateCustomEvent()
    {
        if(!TextUtils.isEmpty(editTextEventName.getText().toString()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private boolean validateCustomPageEvent()
    {
        if(!TextUtils.isEmpty(editTextPageName.getText().toString()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private boolean validateCustomData1()
    {
        if(!TextUtils.isEmpty(editTextKey1.getText().toString()) && !TextUtils.isEmpty(editTextValue1.getText().toString()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private boolean validateCustomData2()
    {
        if(!TextUtils.isEmpty(editTextKey2.getText().toString()) && !TextUtils.isEmpty(editTextValue2.getText().toString()))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
