package com.humuson.tasdemo.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;

import com.apms.sdk.TAS;
import com.apms.sdk.api.APIManager;
import com.apms.sdk.bean.TASModelLogin;
import com.apms.sdk.bean.TASModelUserProperty;
import com.humuson.tasdemo.R;
import com.humuson.tasdemo.helper.PreferenceHelper;
import com.humuson.tasdemo.presenter.AccountEventPresenter;

import org.json.JSONObject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AccountEventActivity extends AppCompatActivity
{
    @BindView(R.id.account_linearlayout_container) LinearLayout linearLayoutContainer;
    @BindView(R.id.account_spinner) Spinner spinner;
    @BindView(R.id.account_edittext_email) EditText editTextEmail;
    @BindView(R.id.account_edittext_name) EditText editTextName;
    @BindView(R.id.account_edittext_phone) EditText editTextPhone;
    @BindView(R.id.account_edittext_userId) EditText editTextUserId;
    @BindView(R.id.account_switch_alimTalk) Switch switchAlimTalk;
    @BindView(R.id.account_switch_email) Switch switchEmail;
    @BindView(R.id.account_switch_push) Switch switchPush;
    @BindView(R.id.account_switch_sms) Switch switchSms;
    @BindView(R.id.account_switch_webPush) Switch switchWebPush;

    private AccountEventPresenter presenter;
    private TAS tas;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_account_activity);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter = new AccountEventPresenter(this);
        spinner.setAdapter(presenter.getSpinnerAdapter(presenter.getEventList()));
        spinner.setOnItemSelectedListener(presenter.getSpinnerSelectListener());
        tas = TAS.getInstance(getApplicationContext());
        editTextUserId.setText(PreferenceHelper.getInstance(getApplicationContext()).getUserId());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    @OnClick({
            R.id.account_button_call,
            R.id.account_button_login
             }) void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.account_button_login:
                if(validateEvent())
                {
                    TASModelLogin params = new TASModelLogin(editTextUserId.getText().toString());
                    tas.tasLogin(params, new APIManager.APICallback() {
                        @Override
                        public void response(String s, JSONObject jsonObject)
                        {
                            presenter.showSnackBar("성공 ["+s+"]",linearLayoutContainer);
                        }
                    });
                    PreferenceHelper.getInstance(getApplicationContext()).setUserId(editTextUserId.getText().toString());
                }
                else
                {
                    presenter.showSnackBar("userId 값을 입력해주세요", linearLayoutContainer);
                }
                break;
            case R.id.account_button_call:
                if(validateEvent())
                {
                    TASModelUserProperty params = new TASModelUserProperty.Builder()
                            .setEmailFlag(switchEmail.isChecked())
                            .setSmsFlag(switchSms.isChecked())
                            .setPushFlag(switchPush.isChecked())
                            .setWebPushFlag(switchWebPush.isChecked())
                            .setName(editTextName.getText().toString())
                            .setPhone(editTextPhone.getText().toString())
                            .setEmail(editTextEmail.getText().toString())
                            .build(editTextUserId.getText().toString());
                    switch(presenter.getSelectedEvent())
                    {
                        case AccountEventPresenter.EVENT_SIGNUP:
                            tas.tasSignUp(params, new APIManager.APICallback() {
                                @Override
                                public void response(String s, JSONObject jsonObject)
                                {
                                    presenter.showSnackBar("성공 ["+s+"]",linearLayoutContainer);
                                }
                            });
                            break;
                        case AccountEventPresenter.EVENT_USERINFOMODIFY:
                            tas.tasUserInfoModify(params, new APIManager.APICallback() {
                                @Override
                                public void response(String s, JSONObject jsonObject)
                                {
                                    presenter.showSnackBar("성공 ["+s+"]",linearLayoutContainer);
                                }
                            });
                            break;
                    }
                }
                else
                {
                    presenter.showSnackBar("userId 값을 입력해주세요", linearLayoutContainer);
                }

                break;
        }
    }
    private boolean validateEvent()
    {
        if(TextUtils.isEmpty(editTextUserId.getText().toString()))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
