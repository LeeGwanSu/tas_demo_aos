package com.humuson.tasdemo.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.apms.sdk.TAS;
import com.apms.sdk.api.APIManager;
import com.apms.sdk.bean.TASModelCart;
import com.apms.sdk.bean.TASModelCartAbandon;
import com.apms.sdk.bean.TASModelProduct;
import com.humuson.tasdemo.R;
import com.humuson.tasdemo.helper.PreferenceHelper;
import com.humuson.tasdemo.presenter.CartEventPresenter;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CartEventActivity extends AppCompatActivity
{
    @BindView(R.id.cart_spinner) Spinner spinner;
    @BindView(R.id.cart_linearlayout_container) LinearLayout linearLayoutContainer;
    @BindView(R.id.cart_edittext_productName) EditText editTextProductName;
    @BindView(R.id.cart_edittext_productAmount) EditText editTextProductAmount;
    @BindView(R.id.cart_edittext_productCode) EditText editTextProductCode;
    @BindView(R.id.cart_edittext_productImgUrl) EditText editTextProductImgUrl;
    @BindView(R.id.cart_edittext_productPrice) EditText editTextProductPrice;
    @BindView(R.id.cart_edittext_productUrl) EditText editTextProductUrl;
    @BindView(R.id.cart_edittext_categories) EditText editTextCategories;
    @BindView(R.id.cart_edittext_tags) EditText editTextTags;

    private CartEventPresenter presenter;
    private TAS tas;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_cart_activity);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter = new CartEventPresenter(this);
        spinner.setAdapter(presenter.getSpinnerAdapter(presenter.getEventList()));
        spinner.setOnItemSelectedListener(presenter.getSpinnerSelectListener());
        tas = TAS.getInstance(getApplicationContext());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    @OnClick(R.id.cart_button_call) void onClick()
    {
        if(validateEvent())
        {
            TASModelProduct product = new TASModelProduct.Builder()
                    .setProductCode(editTextProductCode.getText().toString())
                    .setProductPrice(editTextProductPrice.getText().toString())
                    .setProductUrl(editTextProductUrl.getText().toString())
                    .setProductImg(editTextProductImgUrl.getText().toString())
                    .build(editTextProductName.getText().toString());
            List<TASModelProduct> productList = new ArrayList<>();
            productList.add(product);
            switch (presenter.getSelectedEvent())
            {
                case CartEventPresenter.EVENT_CART:
                {
                    TASModelCart param = new TASModelCart.Builder()
                            .setProducts(productList)
                            .setCategories(presenter.getCategoryList(editTextCategories.getText().toString()))
                            .setTags(presenter.getTagList(editTextTags.getText().toString()))
                            .setUserId(PreferenceHelper.getInstance(getApplicationContext()).getUserId())
                            .build();
                    tas.tasCartEvent(param, new APIManager.APICallback() {
                        @Override
                        public void response(String s, JSONObject jsonObject)
                        {
                            presenter.showSnackBar("성공 ["+s+"]",linearLayoutContainer);
                        }
                    });
                    break;
                }
                case CartEventPresenter.EVENT_CART_ABANDON:
                {
                    TASModelCartAbandon param = new TASModelCartAbandon.Builder()
                            .setProducts(productList)
                            .setCategories(presenter.getCategoryList(editTextCategories.getText().toString()))
                            .setTags(presenter.getTagList(editTextTags.getText().toString()))
                            .setUserId(PreferenceHelper.getInstance(getApplicationContext()).getUserId())
                            .build();
                    tas.tasCartAbandonEvent(param, new APIManager.APICallback() {
                        @Override
                        public void response(String s, JSONObject jsonObject)
                        {
                            presenter.showSnackBar("성공 ["+s+"]",linearLayoutContainer);
                        }
                    });
                    break;
                }
            }
        }
        else
        {
            presenter.showSnackBar("userId 값을 입력해주세요", linearLayoutContainer);
        }
    }

    private boolean validateEvent()
    {
        if(TextUtils.isEmpty(editTextProductName.getText().toString()))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
